# README #

## What is Tenderizer? ##

Tenderizer is a tool for answering Tender questions in a organised manner, drawing on answers from previous tenders.  
The tool is publicly hosted on http://tenderizer.sitecoreaustralia.com.au  
Contact Sitecore AU for login details

## Who do I talk to? ##

*Contact Dan McGuane or Thomas Eldblom for more info*