﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Tenderize.Startup))]
namespace Tenderize
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
