﻿$(document).ready(function () {
    setTimeout(function () { get_index(); }, 10000);
});

function get_index() {
    $.ajax({
        type: "GET",
        url: "/Solr/Progress"
    }).success(function(results) {
        $("#reindex_progress").html(results);
        setTimeout(function() { get_index(); }, 10000); 
    });
}
