﻿var loadTime;
function toggle(id) {
    if ($('#' + id).css('display') == 'none') {
        $('#' + id).show('slow');
    } else {
        $('#' + id).hide('slow');
    }
}

function toggleQuestion() {
  if ($('#questionColumn').css('display') == 'none')
  {
    toggleAnswer();
    $('#questionColumn').show('slow');
  }
  else
  {
    $('#questionColumn').hide('slow', toggleAnswer );
  }
}

function toggleAnswer() {
  $('#answerColumn').toggleClass('col-md-12');
  $('#answerColumn').toggleClass('col-md-6');
}

function useAnswer(id) {
    if ($('#textAdd').val() == "") {
        $('#textAdd').val($('#' + id).text());
        $('#hdnAnswerId').val(id);
    } else {
        $('#textAdd').val($('#textAdd').val() + "\r\n\r\n" + $('#' + id).text());
    }
}

function show(id) {
    if ($('#' + id).css('display') == 'none') {
        $('#' + id).show('slow');
    }
}

function hide(id) {
    if ($('#' + id).css('display') != 'none') {
        $('#' + id).hide('slow');
    }
}

function toggleEnable(id, parent) {
    var itm = $('#' + id);
    if (parent.checked) {
        $('#' + id).removeAttr('disabled');
    } else {
        $('#' + id).attr('disabled', 'disabled');
    }
}

function refresh() {
    if ($('#searchText').val() == '') {
        location.reload();
    } else {
    $('#form0').submit();
    }
}


function copy(id) {
    if (window.clipboardData && clipboardData.setData) {
        var str = $('#' + id + '_p').text();        
        window.clipboardData.setData('Text', str);        
    }
}

 function showError(id, txt) {
     $('#' + id).text(txt);
 }


 function counter(startingSeconds, id) {
     this.timeLeft = startingSeconds;
     this.element = id;
     this.keepCounting = 1;
     this.timeoutMessage = 'No time left for JavaScript countdown!';

     this.countdown = function() {
         if (this.timeLeft < 2) {
             this.keepCounting = 0;
         }
         this.timeLeft = this.timeLeft - 1;
     };

     this.formatOutput = function (t) {
        
     };

     this.showTimeLeft = function (t) {
         $('#' + this.element).html(this.formatOutput(t));
     };

     this.noTimeLeft = function() {
         $('#' + this.element).html(this.timeoutMessage);
     };

     this.count = function () {
         this.countdown();
         this.showTimeLeft(this.timeLeft);
     };

     this.timer = function () {
         this.count();
         if (this.keepCounting) {
             setTimeout(this.timer(), 1000);
         } else {
             this.noTimeLeft();
         }
     };

     this.init = function () {
         this.timer();
     };
 }

 
 function addLeadingZero(n) {
     if (n.toString().length < 2) {
         return '0' + n;
     } else {
         return n;
     }
 }
 
 function getValue(hid) {
     return $('#' + hid).val();
 }
 
 function setValue(hid, str) {
     $('#' + hid).val(str);
 }

 function formatOutput(output) {
     if (output < 1)
         return 'Finished!';

     var hours, minutes, seconds;
     var remain = output;
     seconds = remain % 60;
     minutes = Math.floor(remain / 60) % 60;
     hours = Math.floor(remain / 3600);

     seconds = addLeadingZero(seconds);
     minutes = addLeadingZero(minutes);
     hours = addLeadingZero(hours);

     return hours + ':' + minutes + ':' + seconds;
 }

 function clearval(id) {
     $("#" + id).val("");
 }
 

 $(document).ready(function () {

     //Navigation Menu Slider
     $('#nav-expander').on('click', function (e) {
         e.preventDefault();
         $('body').toggleClass('nav-expanded');
     });
     $('#nav-close').on('click', function (e) {
         e.preventDefault();
         $('body').removeClass('nav-expanded');
     });

     // Initialize navgoco with default options
     $(".main-menu").navgoco({
         caret: '<span class="caret"></span>',
         accordion: false,
         openClass: 'open',
         save: true,
         cookie: {
             name: 'navgoco',
             expires: false,
             path: '/'
         },
         slide: {
             duration: 300,
             easing: 'swing'
         }
     });

 });

// var counter = new counter(36000, 'time');
// counter.init();
// $(document).ready(setupPopup());
// //$(document).ready(function () {
// function setupPopup(){
//     //When you click on a link with class of poplight and the href starts with a # 
//     $('a.poplight[href^=#]').click(function () {
//         var popID = $(this).attr('rel'); //Get Popup Name
//         var popURL = $(this).attr('href'); //Get Popup href to define size
//         var id = $(this).attr('id');
//         $("#guidAdd").val(id);

//         //Pull Query & Variables from href URL
//         var query = popURL.split('?');
//         var dim = query[1].split('&');
//         var popWidth = dim[0].split('=')[1]; //Gets the first query string value
//         $('#entryFor').html($('#q_' + id).html());

//         //Fade in the Popup and add close button
//         $('#' + popID).fadeIn().css({ 'width': Number(popWidth) }).prepend('<a href="#" class="close"><img src="/content/images/close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>');

//         //Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
//         var popMargTop = ($('#' + popID).height() + 80) / 2;
//         var popMargLeft = ($('#' + popID).width() + 80) / 2;

//         //Apply Margin to Popup
//         $('#' + popID).css({
//             'margin-top': -popMargTop,
//             'margin-left': -popMargLeft
//         });

//         //Fade in Background
//         $('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
//         $('#fade').css({ 'filter': 'alpha(opacity=80)' }).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 

//         return false;
//     });

//     //Close Popups and Fade Layer
//     $('a.close, #fade').live('click', function () { //When clicking on the close or fade layer...
//         $('#textAdd').val('');
//         $('#fade , .popup_block').fadeOut(function () {
//             $('#fade, a.close').remove();  //fade them both out
//         });

//         return false;
//     });
// };