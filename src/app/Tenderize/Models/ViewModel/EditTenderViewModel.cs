﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Tenderize.Models.ViewModel
{
    public class EditTenderViewModel
    {
        public EditTenderViewModel() { }

        public EditTenderViewModel(Tender tender, TenderizeDbContext db)
        {
            Id = tender.Id;
            Name = tender.Name;
            Tags = db.MetaTags.ToDictionary(tag => tag.Id,
                tag => new Tuple<string, bool>(tag.Value, tender.MetaTags.Contains(tag)));

        }

        [HiddenInput]
        public int Id { get; set; }

        [Required]
        [DisplayName("Tender Name")]
        public string Name { get; set; }

        public IDictionary<int, Tuple<string, bool>> Tags { get; set; }
    }
}