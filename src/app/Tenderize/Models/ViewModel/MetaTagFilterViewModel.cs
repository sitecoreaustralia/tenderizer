﻿using System;
using System.Collections.Generic;

namespace Tenderize.Models.ViewModel
{
    public class MetaTagFilterViewModel
    {
        public IDictionary<int, Tuple<string, bool>> MetaTags { get; set; }
    }
}