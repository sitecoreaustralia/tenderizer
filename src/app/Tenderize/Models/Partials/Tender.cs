﻿using System;
using System.Linq;

namespace Tenderize.Models
{
    public partial class Tender: IEntity
    {
        
        public TenderQuestion FirstUnanswered
        {
            get
            {
                return TenderQuestions.Where(t => t.Answer == null).OrderBy(t => t.TenderSequence).FirstOrDefault() ??
                         TenderQuestions.OrderBy(t => t.TenderSequence).FirstOrDefault();

            }
        }

        public int UnansweredQuestionsCount
        {
            get { return TenderQuestions.Count(t => t.QuestionAnswer.Answer == null); }
        }

        public TimeSpan TimeRemaining
        {
            get { return new TimeSpan(0, 0, 0, UnansweredQuestionsCount * 200); }
        }

    }
}