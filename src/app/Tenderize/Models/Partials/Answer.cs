﻿using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Ajax.Utilities;
using SolrNet.Attributes;

namespace Tenderize.Models
{
    public partial class Answer : IRelational, IEntity
    {
        public const string SearchResultType = "Answer";

        public static Answer ById(string id)
        {
            int parsed;
            return (!int.TryParse(id, out parsed) ? null : ById(parsed));
        }

        public static Answer ById(int id)
        {
            return new TenderizeDbContext().Answers.FirstOrDefault(a => (a.Id == id));
        }

        public double Score
        {
            get { return Votes.Sum(v => v.Score); }
        }

        public Collection<IRelational> RelatedItems
        {
            get { return new Collection<IRelational>((QuestionAnswers.Where(q => q.Question != null).Select(q => q.Question)).ToList<IRelational>()); }
        }


        [SolrField("content")]
        public string SolrContent
        {
            get { return Text; }
        }

        [SolrUniqueKey("entitytype")]
        public string SolrEntityType
        {
            get { return SearchResultType; }
        }

        [SolrUniqueKey("id")]
        public string SolrId
        {
            get { return Id.ToString(); }
        }

        [SolrUniqueKey("related")]
        public string SolrRelated
        {
            get { return string.Join(",", from r in RelatedItems select r.Id.ToString()); }
        }

        public double? SolrBoost
        {
            get { return Score; }
        }

        [SolrField("tags")]
        public string SolrTags
        {
            get { return string.Join(",", from t in Tags select t.Text); }
        }
    }
}