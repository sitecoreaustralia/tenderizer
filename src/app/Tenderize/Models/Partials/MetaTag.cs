﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Tenderize.Models
{
    public partial class MetaTag
    {
        /// <summary>
        /// Pass a list of metatag strings and they will be either created if required then returned
        /// </summary>
        /// <param name="metaTags">List of MetaTag strings</param>
        /// <param name="dbContext">dbContext for data access</param>
        /// <returns></returns>
        public static List<MetaTag> Add(List<string> metaTags, TenderizeDbContext dbContext)
        {
            var result = new List<MetaTag>();
            foreach (var metaTag in metaTags)
            {
                var metaTagLower = metaTag.ToLower();
                var mTag = dbContext.MetaTags.FirstOrDefault(m => m.Value == metaTagLower);
                if (mTag == null)
                {
                    mTag = new MetaTag { Value = metaTagLower };
                    dbContext.MetaTags.Add(mTag);
                }
                result.Add(mTag);
            }

            dbContext.SaveChanges();
            return result;
        }

        public async static Task<List<MetaTag>> AddAsync(List<string> metaTags, TenderizeDbContext dbContext)
        {
            var result = new List<MetaTag>();
            foreach (var metaTag in metaTags)
            {
                var metaTagLower = metaTag.ToLower();
                var mTag = await dbContext.MetaTags.FirstOrDefaultAsync(m => m.Value == metaTagLower);
                if (mTag == null)
                {
                    mTag = new MetaTag { Value = metaTagLower };
                    dbContext.MetaTags.Add(mTag);
                }
                result.Add(mTag);
            }

            dbContext.SaveChanges();
            return result;
        }

    }
}