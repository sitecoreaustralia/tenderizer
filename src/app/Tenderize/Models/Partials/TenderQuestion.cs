﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tenderize.Models
{
    public partial class TenderQuestion
    {
        public int NextTenderQuestion()
        {
            var list = new LinkedList<TenderQuestion>(Tender.TenderQuestions.OrderBy(q => q.TenderSequence));
            var node = list.Find(this);
            if (node == null) return 0;
            var next = node.Next;
            return next == null ? 0 : next.Value.Id;
        }

        public bool HasAnswer
        {
            get { return Answer != null; }
        }

        public int PreviousTenderQuestion()
        {
            var list = new LinkedList<TenderQuestion>(Tender.TenderQuestions.OrderBy(q => q.TenderSequence));
            var node = list.Find(this);
            if (node == null) return 0;
            var previous = node.Previous;
            return previous == null ? 0 : previous.Value.Id;
        }

        public TenderQuestion NextUnanswered
        {
            get
            {
                var nextUnansweredQuestions = Tender.TenderQuestions.OrderBy(q => q.TenderSequence)
                    .Where(question => question.TenderSequence > TenderSequence && question.QuestionAnswer.Answer == null);
                return nextUnansweredQuestions.FirstOrDefault() ?? this;
            }
        }

        public Question Question
        {
            get { return QuestionAnswer.Question; }
        }

        public Answer Answer
        {
            get { return QuestionAnswer.Answer; }
        }

    }
}