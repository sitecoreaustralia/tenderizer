﻿using System.Collections.ObjectModel;
using System.Linq;
using SolrNet.Attributes;

namespace Tenderize.Models
{
    public partial class Question : IRelational, IEntity
    {
        public const string SearchResultType = "Question";

        public static Question ById(string id)
        {
            int parsed;
            return (!int.TryParse(id, out parsed) ? null : ById(parsed));
        }

        public static Question ById(int id)
        {
            return new TenderizeDbContext().Questions.FirstOrDefault(a => (a.Id == id));
        }

        public Collection<IRelational> RelatedItems
        {
            get { return new Collection<IRelational>((QuestionAnswers.Where(a => a.Answer != null).Select(q => q.Answer)).ToList<IRelational>()); }
        }

        public bool HasAnswer
        {
            get { return QuestionAnswers.Any(a => a.Answer != null); } 
        }

        [SolrField("content")]
        public string SolrContent
        {
            get { return Text; }
        }

        [SolrUniqueKey("entitytype")]
        public string SolrEntityType
        {
            get { return SearchResultType; }
        }

        [SolrUniqueKey("id")]
        public string SolrId
        {
            get { return Id.ToString(); }
        }

        [SolrUniqueKey("related")]
        public string SolrRelated
        {
            get { return string.Join(",", from r in RelatedItems select r.Id.ToString()); }
        }

        public double? SolrBoost
        {
            get { return null; }
        }

        [SolrField("tags")]
        public string SolrTags
        {
            get { return string.Join(",", Tags.Select(t => t.Text)); }
        }
    }
}