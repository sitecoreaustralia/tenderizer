﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace Tenderize.Models
{
    public class TenderizeDbContext : TenderizeEntities
    {
        public override int SaveChanges()
        {
            //you may need this line depending on your exact configuration
            //ChangeTracker.DetectChanges();
            foreach (DbEntityEntry o in GetChangedEntries())
            {
                IEntity entity = o.Entity as IEntity;
                if (entity != null)
                {
                    if (o.State == EntityState.Added)
                    {
                        entity.CreatedBy = HttpContext.Current.User.Identity.GetUserName();
                        entity.CreatedDate = DateTime.Now;
                    }
                    else
                    {
                        entity.ModifiedBy = HttpContext.Current.User.Identity.Name;
                        entity.ModifiedDate = DateTime.Now;
                    }
                }
            }
            return base.SaveChanges();
        }

        private IEnumerable<DbEntityEntry> GetChangedEntries()
        {
            return new List<DbEntityEntry>(
                from e in ChangeTracker.Entries()
                where e.State != EntityState.Unchanged
                select e);
        }
    }
}