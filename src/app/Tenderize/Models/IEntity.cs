﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tenderize.Models
{
    public interface IEntity
    {
        string ModifiedBy { get; set; }
        DateTime? ModifiedDate { get; set; }
        string CreatedBy { get; set; }
        DateTime CreatedDate { get; set; }
    }
}