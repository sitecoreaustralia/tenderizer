﻿using System.Collections.ObjectModel;

namespace Tenderize.Models
{
    public interface IRelational
    {
        int Id { get; set; }

        Collection<IRelational> RelatedItems { get; }

        string SolrContent { get; }

        string SolrEntityType { get; }

        string SolrId { get; }

        string SolrRelated { get; }

        double? SolrBoost { get; }

        string Text { get; set; }
    }
}