﻿#region using

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using Tenderize.Framework.Attributes;
using Tenderize.Framework.Helpers;
using Tenderize.Framework.Solr;
using Tenderize.Models;
using Tenderize.Models.ViewModel;

#endregion

namespace Tenderize.Controllers
{
    [Authorize(Roles = "admin, contributor")]
    public class TenderizeController : Controller
    {
        private static readonly TenderizeDbContext Db = new TenderizeDbContext();

        public ActionResult Index()
        {
            var tenders = Db.Tenders.OrderByDescending(t => t.Date);
            return View(tenders);
        }

        private static string ConvertSymbols(string importString)
        {
            return importString.Replace("@@@@", Environment.NewLine).Trim();
        }

        [HttpPost]
        public ActionResult Create(string name)
        {
            var entity = new Tender
            {
                Name = name,
                Date = DateTime.Now
            };
            Db.Tenders.Add(entity);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            Db.Tenders.Remove(Db.Tenders.Single(a => a.Id == id));
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Add(string answerText, int id, int nextQuestionId, string submit, int? hdnAnswerId)
        {
            var tenderQuestion = Db.TenderQuestions.Single(q => q.Id == id);
            answerText = answerText.Trim();

            if (!string.IsNullOrWhiteSpace(answerText))
            {
                if (!hdnAnswerId.HasValue)
                {
                    if (tenderQuestion.Answer != null)
                    {
                        //update
                        tenderQuestion.Answer.Text = answerText;
                    }
                    else
                    {
                        //insert
                        tenderQuestion.QuestionAnswer.Answer = new Answer {Text = answerText};
                    }
                }

                else
                {
                    var existingAnswer = Db.Answers.SingleOrDefault(a => a.Id == hdnAnswerId.Value);
                    if (existingAnswer == null)
                    {
                        tenderQuestion.QuestionAnswer.Answer = new Answer {Text = answerText};
                    }
                    else
                    {
                        if (existingAnswer == tenderQuestion.Answer)
                        {
                            //answer used == tender answer already set - weird scenario
                            if (tenderQuestion.Answer.Text.Trim() == answerText)
                            {
                                //nothing to do
                            }
                            else
                            {
                                //answer selected but text has changed - default to new answer
                                tenderQuestion.QuestionAnswer.Answer = new Answer {Text = answerText};
                            }
                        }
                        else
                        {
                            //answer used != tender answer already set
                            if (existingAnswer.Text.Trim() == answerText)
                            {
                                //existing answer used and no changes made - update reference to existing answer
                                tenderQuestion.QuestionAnswer.Answer = existingAnswer;
                            }
                            else
                            {
                                //existing answer used but changes were made - default to new answer
                                //todo: consider versioning answers
                                tenderQuestion.QuestionAnswer.Answer = new Answer {Text = answerText};
                            }
                        }
                    }
                }
            }
            else
            {
                //empty answertext
                if (tenderQuestion.Answer != null)
                {
                    tenderQuestion.QuestionAnswer.Answer = null;
                    //todo: consider deleting answer if only used for this tender
                    //scenario: placeholder answer set and then cleared
                }
            }

            Db.SaveChanges();

            if (submit == "next")
            {
                return RedirectToAction("Process", new
                {
                    id = nextQuestionId.ToString()
                });
            }
            return RedirectToAction("Process", new
            {
                id
            });
        }

        public ActionResult DeleteTag(int tagid, int questionid)
        {
            var question = Db.Questions.Single(q => q.Id == questionid);
            question.Tags.Remove(Db.Tags.Single(a => a.Id == tagid));
            Db.SaveChanges();
            return RedirectToAction("Process", new
            {
                id = question.Id.ToString()
            });
        }

        public ActionResult Export(int id)
        {
            var model = Db.Tenders.Single(a => a.Id == id);
            var info = new FileInfo(Path.Combine(Server.MapPath("~/App_Data/uploads"), "export_" + model.Id + ".xlsx"));
            if (!info.Exists)
                return View("Import", model);
            var destFileName = Path.Combine(Server.MapPath("~/App_Data/uploads"), "final_" + model.Id + ".xlsx");
            info.CopyTo(destFileName, true);
            var newFile = new FileInfo(destFileName);
            using (var package = new ExcelPackage(newFile))
            {
                foreach (var tenderQuestion in model.TenderQuestions)
                {
                    var answer = tenderQuestion.Answer;
                    if (answer == null) continue;
                    int num;
                    int num2;
                    int num3;
                    if (string.IsNullOrEmpty(tenderQuestion.ExcelAnswerLocation))
                    {
                        var strArray = tenderQuestion.ExcelQuestionLocation.Split('-');
                        num = int.Parse(strArray[0]);
                        num2 = int.Parse(strArray[1]);
                        num3 = int.Parse(strArray[2]) + 1;
                    }
                    else
                    {
                        var strArray2 = tenderQuestion.ExcelAnswerLocation.Split('-');
                        num = int.Parse(strArray2[0]);
                        num2 = int.Parse(strArray2[1]);
                        num3 = int.Parse(strArray2[2]);
                    }
                    package.Workbook.Worksheets[num].Cells[num2, num3].Value = answer.Text;
                }
                package.Save();
            }
            return File(destFileName, "APPLICATION/OCTET-STREAM", model.Name + ".xlsx");
        }

        public async Task<ActionResult> Import(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var tender = await Db.Tenders.FindAsync(id);
            if (tender == null)
            {
                return HttpNotFound();
            }

            var viewModel = new EditTenderViewModel(tender, Db);


            ViewBag.ImportFileExists =
                System.IO.File.Exists(Path.Combine(Server.MapPath("~/App_Data/uploads"), "import_" + id + ".xlsx"));
            ViewBag.ExportFileExists =
                System.IO.File.Exists(Path.Combine(Server.MapPath("~/App_Data/uploads"), "export_" + id + ".xlsx"));
            return View(viewModel);
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Save")]
        public async Task<ActionResult> Save(EditTenderViewModel tender)
        {
            if (!ModelState.IsValid) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var metaTags = await ExtractTags(Request);

            var dbTender = await Db.Tenders.FindAsync(tender.Id);
            dbTender.MetaTags.Clear();

            foreach (var metaTag in metaTags)
            {
                dbTender.MetaTags.Add(metaTag);
            }
            await Db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpPost]
        [MultipleButton(Name="action", Argument = "Import")]
        public async Task<ActionResult> Import(Tender tender)
        {
            if (!ModelState.IsValid) return RedirectToAction("Index");
            
            var newFile =
                new FileInfo(Path.Combine(Server.MapPath("~/App_Data/uploads"), "import_" + tender.Id + ".xlsx"));
            if (!newFile.Exists)
                return View("Import", tender);

            var metaTags = await ExtractTags(Request);

            foreach (var metaTag in metaTags)
            {
                tender.MetaTags.Add(metaTag);
            }
            using (var package = new ExcelPackage(newFile))
            {
                var num = 0;
                foreach (var worksheet in package.Workbook.Worksheets)
                {
                    if (worksheet.Dimension != null)
                    {
                        var styleXfId = package.Workbook.Styles.NamedStyles.First(ns => (ns.Name == "Good")).StyleXfId;
                        var num3 = package.Workbook.Styles.NamedStyles.First(ns => (ns.Name == "Bad")).StyleXfId;
                        for (var i = 1; i <= worksheet.Dimension.End.Row; i++)
                        {
                            var questionText = "";
                            var answerText = "";
                            var questionLocation = "";
                            var answerLocation = "";
                            for (var j = 1; j <= worksheet.Dimension.End.Column; j++)
                            {
                                if (worksheet.Cells[i, j].Style.XfId == styleXfId)
                                {
                                    if (!string.IsNullOrEmpty(questionText))
                                        return
                                            Content(
                                                string.Format(
                                                    "Row {0} in Worksheet {1}:{2} has more than one question column marked.",
                                                    i, worksheet.Index, worksheet.Name));
                                    questionText = (worksheet.Cells[i, j].Value == null)
                                        ? string.Empty
                                        : worksheet.Cells[i, j].Value.ToString();
                                    questionLocation = string.Format("{0}-{1}-{2}", worksheet.Index, i, j);
                                }
                                if (worksheet.Cells[i, j].Style.XfId != num3) continue;
                                if (!string.IsNullOrEmpty(answerText))
                                    return
                                        Content(
                                            string.Format(
                                                "Row {0} in Worksheet {1}:{2} has more than one answer column marked.",
                                                i, worksheet.Index, worksheet.Name));
                                answerText = (worksheet.Cells[i, j].Value == null)
                                    ? string.Empty
                                    : worksheet.Cells[i, j].Value.ToString();
                                answerLocation = string.Format("{0}-{1}-{2}", worksheet.Index, i, j);
                            }
                            if (!(string.IsNullOrEmpty(questionText) || !string.IsNullOrEmpty(answerLocation)))
                                return
                                    Content(string.Format("Row {0} in Worksheet {1}:{2} has no answer column marked.", i,
                                        worksheet.Index, worksheet.Name));
                            if (string.IsNullOrEmpty(questionText)) continue;

                            questionText = ConvertSymbols(questionText.Trim());

                            //Todo: cleanup - check for uniqueness of question - maybe add similar/related
                            //check for existing question by exact match
                            //maybe move this to checksum in the database for faster matching?

                            var question = Db.Questions.FirstOrDefault(q => q.Text == questionText);
                            if (question == null)
                            {
                                question = new Question
                                {
                                    Text = questionText
                                };
                                foreach (var metaTag in metaTags)
                                {
                                    question.MetaTags.Add(metaTag);
                                }
                                Db.Questions.Add(question);

                            }

                            var questionAnswer = new QuestionAnswer();
                            question.QuestionAnswers.Add(questionAnswer);

                            var tenderQuestion = new TenderQuestion
                            {
                                TenderId = tender.Id,
                                TenderSequence = num + 1,
                                ExcelQuestionLocation = questionLocation,
                                ExcelAnswerLocation = answerLocation
                            };
                            questionAnswer.TenderQuestions.Add(tenderQuestion);


                            if (!String.IsNullOrWhiteSpace(answerText))
                            {
                                var answer = new Answer
                                {
                                    Text = ConvertSymbols(answerText.Trim())
                                };
                                foreach (var metaTag in metaTags)
                                {
                                    answer.MetaTags.Add(metaTag);
                                }
                                questionAnswer.Answer = answer;
                            }
                            num++;
                            //}
                        }
                    }
                }
            }
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ImportFile(HttpPostedFileBase file, Tender tender, string type)
        {
            if ((file == null) || (file.ContentLength <= 0))
                return RedirectToAction("Index");
            var filename = Path.Combine(Server.MapPath("~/App_Data/uploads"),
                string.Concat(type, "_", tender.Id, ".xlsx"));
            file.SaveAs(filename);
            ViewBag.ImportFileExists =
                System.IO.File.Exists(Path.Combine(Server.MapPath("~/App_Data/uploads"), "import_" + tender.Id + ".xlsx"));
            ViewBag.ExportFileExists =
                System.IO.File.Exists(Path.Combine(Server.MapPath("~/App_Data/uploads"), "export_" + tender.Id + ".xlsx"));
            return View("Import", new EditTenderViewModel(tender, Db));
        }

        public ActionResult Process(int? id)
        {
            if (!id.HasValue)
                return RedirectToAction("Index");
            var model = Db.TenderQuestions.FirstOrDefault(a => a.Id == id);
            //var model = Db.Questions.FirstOrDefault(a => a.Id == id);
            if (model == null)
                return RedirectToAction("Index");
            ViewBag.Term = "Enter a search term";
            ViewBag.HasQuery = false;

            if (!model.HasAnswer)
            {
                var exactMatch = (from q in Db.QuestionAnswers
                                where q.Id != model.Id
                                        && q.Answer != null
                                        && q.Question.Text == model.Question.Text
                                orderby q.Answer.Tags.Sum(z => z.Popularity) descending 
                                select q.Answer).FirstOrDefault();
                //var exactMatch = Db.Questions.FirstOrDefault(q => q.Text == model.Question.Text && q.Id != model.QuestionAnswer.QuestionId);
                if (exactMatch != null) ViewBag.ExactMatchAnswer = exactMatch;
            }
            //use model.QuestionAnswer instead of model.Question as Linq may not map the SQL effeciently?
            var tags = from t in Db.Tags where model.QuestionAnswer.Question.Text.Contains(t.Text) select t;
            ViewBag.MatchingTags = tags;
            var str = string.Join(", ", from t in tags select t.Text);
            ViewBag.Term = str;
            ViewBag.Results = SolrUtil.Find(tags);
            return View(model);
        }

        public ActionResult Reindex()
        {
            IndexUtil.ReIndex();
            return RedirectToAction("Index");
        }

        public PartialViewResult Search(string query, int id)
        {
            if (string.IsNullOrEmpty(query))
            {
                ViewBag.Term = "Enter a search term";
                ViewBag.HasQuery = false;
            }
            else
            {
                var tag = Db.Tags.FirstOrDefault(t => t.Text == query);
                if (tag == null)
                {
                    tag = new Tag
                    {
                        Text = query
                    };
                    Db.Tags.Add(tag);
                    Db.SaveChanges();
                }
                var tenderQuestion = Db.TenderQuestions.Single(a => a.Id == id);
                if (tenderQuestion.Question.Tags.FirstOrDefault(t => (t.Id == tag.Id)) == null)
                {
                    tag.Questions.Add(tenderQuestion.Question);
                    Db.Tags.Attach(tag);
                    Db.SaveChanges();
                }
                ViewBag.Term = query;
                ViewBag.HasQuery = true;
                var model = SolrUtil.Find(query);
                return PartialView("_SearchResults", model);
            }
            return PartialView("_SearchResults");
        }

        public PartialViewResult Vote(int id, long? score)
        {
            var answer = Db.Answers.Single(a => a.Id == id);
            var qa = answer.QuestionAnswers.FirstOrDefault();
            if (qa == null) throw new NotImplementedException("Condition not handled - no related questions to Answer");

            if (!score.HasValue)
            {
                return PartialView("_QuestionAnswer", qa.Question);
            }

            var vote = answer.Votes.FirstOrDefault(v => v.Username == User.Identity.Name);
            if (vote == null)
            {
                vote = new Vote
                {
                    AnswerId = id,
                    //Answer = doc,
                    Score = score.Value,
                    Username = User.Identity.Name
                };
                Db.Votes.Add(vote);
            }
            else if (score == 0)
                Db.Votes.Remove(vote);
            else
                vote.Score = score.Value;
            Db.SaveChanges();

            var instance = SolrUtil.GetSolrInstance<IRelational>();
            instance.AddWithBoost(answer, answer.Score*10.0);
            var question = qa.Question;
            instance.AddWithBoost(question, answer.Score*10.0);

            return PartialView("_QuestionAnswer", qa.Question);
        }

        private async Task<IEnumerable<MetaTag>> ExtractTags(HttpRequestBase request)
        {
            //new keys
            var dynFields = Request.Form.AllKeys.Where(k => k.StartsWith("field")).Select(k => request[k]).Where(s => !String.IsNullOrWhiteSpace(s));
            List<MetaTag> metaTags = new List<MetaTag>();

            var enumerable = dynFields as string[] ?? dynFields.ToArray();
            if (enumerable.Any())
            {
                metaTags = await MetaTag.AddAsync(enumerable.ToList(), Db);
            }

            //existing keys
            var oldKeys =
                Request.Form.AllKeys.Where(k => k.StartsWith("metaTag_") && request[k] == "on")
                    .Select(k => k.Replace("metaTag_", ""));

            foreach (var oldKey in oldKeys)
            {
                int id;
                if (!int.TryParse(oldKey, out id)) continue;

                var metaTag = await Db.MetaTags.FindAsync(id);
                if(metaTag != null) metaTags.Add(metaTag);
            }

            return metaTags;
        } 
    }
}