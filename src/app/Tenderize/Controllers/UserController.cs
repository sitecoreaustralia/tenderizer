﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Tenderize.Framework.Configuration;
using Tenderize.Framework.Helpers;
using Tenderize.Models;

namespace Tenderize.Controllers
{
    [Authorize(Roles = "admin")]
    public class UserController : Controller
    {
        private readonly TenderizeDbContext _db = new TenderizeDbContext();
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        public ViewResult Index()
        {
            var aspUsers = _db.AspNetUsers;
            return View(aspUsers.ToList());
        }

        public async Task<ActionResult> Approve(string id)
        {
            var owinUser = UserManager.FindById(id);
            if (owinUser == null) return RedirectToAction("Index");

            var token = await UserManager.GenerateEmailConfirmationTokenAsync(owinUser.Id);
            await UserManager.ConfirmEmailAsync(id, token);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> ResetPassword(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
            {
                //Todo: show error for user not approved
                throw new NotImplementedException("user not approved scenario");
                // Don't reveal that the user does not exist or is not confirmed
            }

            // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link
            string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            code = HttpUtility.UrlEncode(code);
            var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Unapprove(string id)
        {
            var owinUser = await UserManager.FindByIdAsync(id);
            if (owinUser == null) return RedirectToAction("Index");

            owinUser.EmailConfirmed = false;
            await UserManager.UpdateAsync(owinUser);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> MakeAdmin(string id)
        {
            var owinUser = await UserManager.FindByIdAsync(id);
            if (owinUser == null) return RedirectToAction("Index");

            await UserManager.AddToRoleAsync(owinUser.Id, Strings.AdminRole);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> RemoveAdmin(string id)
        {
            var owinUser = await UserManager.FindByIdAsync(id);
            if (owinUser == null) return RedirectToAction("Index");
            await UserManager.RemoveFromRoleAsync(owinUser.Id, Strings.AdminRole);
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> AddToRole(string id, string role)
        {
            var owinUser = await UserManager.FindByIdAsync(id);
            if (owinUser == null) return RedirectToAction("Index");
            await UserManager.AddToRoleAsync(owinUser.Id, role);
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> RemoveRole(string id, string role)
        {
            var owinUser = await UserManager.FindByIdAsync(id);
            if (owinUser == null) return RedirectToAction("Index");
            await UserManager.RemoveFromRoleAsync(owinUser.Id, role);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }
                if (_db != null)
                {
                    _db.Dispose();
                    _userManager = null;
                }
            }

            base.Dispose(disposing);
        }

        public ActionResult Unlock(string id)
        {
            throw new NotImplementedException();
            var user = _db.AspNetUsers.Single(a => a.Id == id);
            var mem = Membership.GetUser(user.UserName);
            if (mem != null)
            {
                mem.UnlockUser();
            }
            return RedirectToAction("Index");
        }

        #region Legacy

        [HttpPost]
        public ActionResult Edit(string userId, string admin, string editor)
        {
            throw new NotImplementedException();
            Guid id;
            if (!Guid.TryParse(userId, out id))
            {
                RedirectToAction("Index");
            }

            var user = _db.AspNetUsers.Single(a => a.Id == id.ToString());
            if (!ModelState.IsValid) return View(user);

            if (admin == "on")
            {
                if (!Roles.IsUserInRole(user.UserName, Strings.AdminRole))
                    Roles.AddUserToRole(user.UserName, Strings.AdminRole);
            }
            if (Roles.IsUserInRole(user.UserName, Strings.AdminRole)) Roles.RemoveUserFromRole(user.UserName, "admin");
            if (editor == "on" && !Roles.IsUserInRole(user.UserName, Strings.ContributorRole))
            {
                Roles.AddUserToRole(user.UserName, Strings.ContributorRole);
            }
            if (editor != "on" && Roles.IsUserInRole(user.UserName, Strings.ContributorRole))
            {
                Roles.RemoveUserFromRole(user.UserName, Strings.ContributorRole);
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(string id)
        {
            throw new NotImplementedException();
            var user = _db.AspNetUsers.Single(a => a.Id == id);
            return View(user);
        }

        public ViewResult Details(string id)
        {
            throw new NotImplementedException();
            var aspNetUser = _db.AspNetUsers.Single(a => a.Id == id);
            return View(aspNetUser);
        }

        public ActionResult Delete(string id)
        {
            throw new NotImplementedException();
            var aspNetUser = _db.AspNetUsers.Single(a => a.Id == id);
            return View(aspNetUser);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            throw new NotImplementedException();
            var aspNetUser = _db.AspNetUsers.Single(a => a.Id == id);
            Membership.DeleteUser(aspNetUser.UserName, true);
            return RedirectToAction("Index");
        }

        #endregion
    }
}