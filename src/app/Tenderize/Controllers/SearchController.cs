﻿#region using

using System;
using System.Linq;
using System.Web.Mvc;
using Tenderize.Framework.Solr;
using Tenderize.Models;

#endregion

namespace Tenderize.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        private static readonly TenderizeDbContext Db = new TenderizeDbContext();

        public ActionResult Index()
        {
            ViewBag.Term = "Enter a search term";
            ViewBag.HasQuery = false;
            return View();
        }

        public PartialViewResult QuestionSearch(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                ViewBag.Term = "Enter a search term";
                ViewBag.HasQuery = false;
            }
            else
            {
                var tags = from t in Db.Tags where query.Contains(t.Text) select t;
                ViewBag.MatchingTags = tags;
                var str = string.Join(", ", from t in tags select t.Text);
                ViewBag.Term = str;
                ViewBag.HasQuery = true;
                var model = SolrUtil.Find(tags);
                return PartialView("_SearchResults", model);
            }
            return PartialView("_SearchResults");
        }

        public PartialViewResult Search(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                ViewBag.Term = "Enter a question";
                ViewBag.HasQuery = false;
            }
            else
            {
                ViewBag.Term = query;
                ViewBag.HasQuery = true;
                var model = SolrUtil.Find(query);
                return PartialView("_SearchResults", model);
            }
            return PartialView("_SearchResults");
        }

        [Authorize]
        public PartialViewResult Vote(int id, long? score)
        {
            throw new NotImplementedException();
            return PartialView("_QuestionAnswer", null);

            //todo: fix voting
            //var doc = _db.Answers.FirstOrDefault(a => a.Id == id);
            //if (score.HasValue)
            //{
            //  Vote entity = doc.Votes.FirstOrDefault<Vote>(v => v.Username == User.Identity.Name);
            //  if (entity == null)
            //  {
            //    entity = new Vote
            //    {
            //      AnswerId = id,
            //      Answer = doc,
            //      Score = score.Value,
            //      Username = User.Identity.Name
            //    };
            //    _db.Votes.Add(entity);
            //  }
            //  else if (score == 0L)
            //    _db.Votes.Remove(entity);
            //  else
            //  {
            //    entity.Score = score.Value;
            //    _db.Entry(entity).CurrentValues.SetValues(entity);
            //  }
            //  _db.SaveChanges();
            //  var instance = SolrUtil.GetSolrInstance<IRelational>();
            //  instance.AddWithBoost(doc, doc.Score*10.0);
            //  var question = doc.Questions.FirstOrDefault();
            //  if (question != null)
            //    instance.AddWithBoost(question, doc.Score*10.0);
            //  instance.Commit();
            //}
            //return PartialView("_QuestionAnswer", doc.Questions.FirstOrDefault<Question>());
        }
    }
}