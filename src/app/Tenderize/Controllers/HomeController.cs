﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Tenderize.Framework.Configuration;
using Tenderize.Models;
using Tenderize.Models.ViewModel;

namespace Tenderize.Controllers
{
    public class HomeController : Controller
    {
        private static readonly TenderizeDbContext Db = new TenderizeDbContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult WaitingConfirm()
        {
            ViewBag.Message =
                String.Format(
                    "Your request has been sent to the administrator.<br />For further information please contact: <a href=\"mailto:{0}\">Tenderizer Admin</a>",
                    Settings.AdminEmailToAddress.Replace("|", ";"));

            return View();
        }

        public ActionResult _MetaTagFilter()
        {
            var metaTags = Db.MetaTags;
            var viewModel = new MetaTagFilterViewModel
            {
                MetaTags = metaTags.ToDictionary(tag => tag.Id,
                    tag => new Tuple<string, bool>(tag.Value, false))
            };
            return PartialView(viewModel);
        }
    }
}