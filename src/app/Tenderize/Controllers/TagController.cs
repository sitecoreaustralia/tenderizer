﻿#region using

using System;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using Tenderize.Framework.Solr;
using Tenderize.Models;

#endregion

namespace Tenderize.Controllers
{
    [Authorize]
    public class TagController : Controller
    {
        private static readonly TenderizeDbContext Db = new TenderizeDbContext();

        public ActionResult Delete(int id)
        {
            var tag = Db.Tags.Single(a => a.Id == id);
            var questions = tag.Questions; //.ToList(); //is tolist required to process the removals? seems ineffecient
            foreach (var question in questions)
                question.Tags.Remove(tag);
            var answers = tag.Answers;
            foreach (var answer in answers)
                answer.Tags.Remove(tag);
            Db.Tags.Remove(Db.Tags.Single(a => a.Id == id));
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Improve(int? id)
        {
            if (!id.HasValue)
                return RedirectToAction("Index");
            var model = Db.Tags.FirstOrDefault(a => a.Id == id);
            if (model == null)
                return RedirectToAction("Index");
            ViewBag.Results = SolrUtil.Find(model.Text);
            return base.View(model);
        }

        public ActionResult Index()
        {
            return View(Db.Tags);
        }

        public void RunPopularityUpdate()
        {
            var tags = Db.Tags;
            foreach (var tag in tags)
            {
                var count = 0;
                try
                {
                    count = SolrUtil.Find(tag.Text).Count;
                }
                catch (Exception)
                {
                }
                tag.Popularity = count;
            }
            Db.SaveChanges();
        }

        public PartialViewResult ToggleBoost(int? id, int? tagId)
        {
            throw new NotImplementedException();
            //var currentEntity = Db.Answers.FirstOrDefault(a => a.Id == id);
            //var entity = Db.Tags.FirstOrDefault(a => a.Id == tagId);
            //if (currentEntity.Tags.Contains(entity))
            //    currentEntity.Tags.Remove(entity);
            //else
            //    currentEntity.Tags.Add(entity);
            //Db.Entry(currentEntity).CurrentValues.SetValues(currentEntity);
            //Db.SaveChanges();
            //var instance = SolrUtil.GetSolrInstance<IRelational>();
            //instance.Add(currentEntity);
            //instance.Commit();
            //ViewBag.Tag = entity;
            //return PartialView("_QuestionAnswer", currentEntity.Questions.FirstOrDefault());
        }

        public ActionResult UpdatePopularity()
        {
            new Thread(RunPopularityUpdate).Start();
            return RedirectToAction("Index");
        }
    }
}