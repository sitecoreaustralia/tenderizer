﻿using System.Web.Mvc;
using Tenderize.Framework.Helpers;

namespace Tenderize.Controllers
{
    [Authorize]
    public class SolrController : Controller
    {
        public PartialViewResult Progress()
        {
            if (!IndexUtil.Indexing)
            {
                return PartialView("Complete");
            }
            ViewBag.Progress = IndexUtil.GetProgress();
            return PartialView("Progress");
        }
    }
}

