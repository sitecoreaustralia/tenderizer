﻿using System.Web.Mvc;
using Tenderize.Framework.Configuration;
using Tenderize.Framework.Filters;

namespace Tenderize
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            if (Settings.ErrorEmailEnabled)
                filters.Add(new ExceptionFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
