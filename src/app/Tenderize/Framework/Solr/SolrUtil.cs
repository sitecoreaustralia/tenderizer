﻿#region using

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using SolrNet;
using SolrNet.Commands.Parameters;
using Tenderize.Framework.Configuration;
using Tenderize.Models;

#endregion

namespace Tenderize.Framework.Solr
{
    public class SolrUtil
    {
        private static bool _initialized;

        public static List<IRelational> Find(string query)
        {
            var solrQuery = new SolrQuery(query);

            return Find(solrQuery, 50);
        }

        private static List<IRelational> Find(ISolrQuery solrQuery, int? maxRows)
        {
            var instance = GetSolrInstance<SolrResult>();
            var options = new QueryOptions
            {
                Rows = maxRows
            };
            var solrResults = instance.Query(solrQuery, options);
            var results = GetFromSolrResults(solrResults);
            return results.ToList();
            //return GetUniqueQuestions(results).ToList();
        }

        private static IEnumerable<IRelational> GetUniqueQuestions(IEnumerable<Question> results)
        {
            return
                (results.GroupBy(question => question.Id).Select(group => @group.FirstOrDefault())).Where(q => q != null);
        }

        public static List<IRelational> Find(IEnumerable<Tag> tags)
        {
            AbstractSolrQuery query = null;
            foreach (var tag in tags)
                query += new SolrQuery(tag.Text);
            return query == null ? new List<IRelational>() : Find(query, 50);
        }

        private static void Initialize()
        {
            if (_initialized)
                return;
            try
            {
                SolrNet.Startup.Init<SolrResult>(Settings.SolrUrl);
                SolrNet.Startup.Init<IRelational>(Settings.SolrUrl);
                SolrNet.Startup.Init<Question>(Settings.SolrUrl);
                SolrNet.Startup.Init<Answer>(Settings.SolrUrl);
                _initialized = true;
            }
            catch (Exception exception)
            {
                var message = exception.Message;
            }
        }

        public static ISolrOperations<T> GetSolrInstance<T>()
        {
            try
            {
                Initialize();
                var instance = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
                return instance;
            }
            catch (Exception e)
            {
                throw new InvalidOperationException("Could not initialize Solr. Is the server running?", e);
            }
        }

        private static IEnumerable<IRelational> GetFromSolrResults(IEnumerable<SolrResult> queryResults)
        {
            try
            {
                var results = new List<IRelational>();
                foreach (var result in queryResults)
                {
                    if (result.SolrEntityType.FirstOrDefault() == Question.SearchResultType)
                    {
                        var question = Question.ById(result.Id);
                        if(question != null) results.Add(question);
                        //AddQuestionToResults(result, results);
                    }
                    if (result.SolrEntityType.FirstOrDefault() != Answer.SearchResultType) continue;

                    var answer = Answer.ById(result.Id);
                    if(answer != null) results.Add(answer);
                    //AddAnswerQuestionToResult(result, results);
                }
                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }

        

        private static void AddAnswerQuestionToResult(SolrResult result, List<Question> results)
        {
            throw new NotImplementedException();
            //var answer = Answer.ById(result.Id.ToString());
            //if (answer == null)
            //  return;
            //var question = answer.Questions.FirstOrDefault();
            //if (question == null)
            //  return;
            //results.Add(question);
        }

        private static void AddQuestionToResults(SolrResult result, List<Question> results)
        {
            throw new NotImplementedException();
            //var question = Question.ById(result.Id.ToString());
            //if (question.HasAnswer)
            //  results.Add(question);
        }
    }
}