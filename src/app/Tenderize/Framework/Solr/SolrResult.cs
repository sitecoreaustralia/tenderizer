﻿using System.Collections.Generic;
using SolrNet.Attributes;

namespace Tenderize.Framework.Solr
{
    public class SolrResult
    {
        [SolrUniqueKey("id")]
        public int Id { get; set; }

        [SolrUniqueKey("entitytype")]
        public IList<string> SolrEntityType { get; set; }
    }
}

