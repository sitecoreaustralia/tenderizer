﻿#region using

using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Tenderize.Framework.Configuration;
using Tenderize.Framework.Helpers;

#endregion

namespace Tenderize.Framework.Filters
{
  public class ExceptionFilter : IExceptionFilter
  {
    public void OnException(ExceptionContext filterContext)
    {
      var ex = filterContext.Exception;
      var num = (ex is HttpException) ? (ex as HttpException).GetHttpCode() : 500;
      if (num != 0x194)
      {
          MailUtil.SendMail(Settings.ErrorEmailSubject, GetHtmlError(ex));
      }
    }

    private static string CleanHtml(string html)
    {
      if (string.IsNullOrEmpty(html))
        return "";
      return ((html.Length == 0) ? "" : html.Replace("<", "<").Replace("\r\n", "<br />").Replace("&", "&").Replace(" ", " "));
    }

    private static string CollectionToHtmlTable(NameValueCollection collection)
    {
      var str = "\n<TABLE width=\"100%\">\n <TR bgcolor=\"#C0C0C0\">" + "<TD><FONT face=\"Arial\" size=\"2\"><!--VALUE--></FONT></TD>".Replace("<!--VALUE-->", " <B>Name</B>") + " " + "<TD><FONT face=\"Arial\" size=\"2\"><!--VALUE--></FONT></TD>".Replace("<!--VALUE-->", " <B>Value</B>") + "</TR>\n";
      if (collection.Count == 0)
      {
        var values = new NameValueCollection();
        values.Add("N/A", "");
        collection = values;
      }
      for (var i = 0; i < collection.Count; i++)
      {
        var str3 = str;
        str = str3 + "<TR valign=\"top\" bgcolor=\"" + (((i%2) == 0) ? "white" : "#EEEEEE") + "\">" + "<TD><FONT face=\"Arial\" size=\"2\"><!--VALUE--></FONT></TD>".Replace("<!--VALUE-->", collection.Keys[i]) + "\n" + "<TD><FONT face=\"Arial\" size=\"2\"><!--VALUE--></FONT></TD>".Replace("<!--VALUE-->", collection[i]) + "</TR>\n";
      }
      return (str + "</TABLE>");
    }

    private static string CollectionToHtmlTable(HttpCookieCollection collection)
    {
      if (collection == null)
        return string.Empty;
      var values = new NameValueCollection();
      foreach (string str in collection)
        values.Add(str, collection[str].Value);
      return CollectionToHtmlTable(values);
    }

    private static string CollectionToHtmlTable(HttpSessionState collection)
    {
      var values = new NameValueCollection();
      foreach (string str in collection)
        values.Add(str, collection[str].ToString());
      return CollectionToHtmlTable(values);
    }

    private static string GetHtmlError(Exception ex)
    {
      var str = ("<FONT face=\"Arial\" size=\"5\" color=\"red\">Error - " + ex.Message + "</FONT><br /><br />") + "<TABLE BORDER=\"0\" WIDTH=\"100%\" CELLPADDING=\"1\" CELLSPACING=\"0\"><TR><TD bgcolor=\"black\" COLSPAN=\"2\"><FONT face=\"Arial\" color=\"white\"><B> <!--HEADER--></B></FONT></TD></TR></TABLE>".Replace("<!--HEADER-->", "Error Information");
      while (ex != null)
      {
        var values2 = new NameValueCollection();
        values2.Add("Message", CleanHtml(ex.Message));
        values2.Add("Source", CleanHtml(ex.Source));
        values2.Add("TargetSite", CleanHtml(ex.TargetSite.ToString()));
        values2.Add("StackTrace", CleanHtml(ex.StackTrace));
        var collection = values2;
        str = str + CollectionToHtmlTable(collection);
        ex = ex.InnerException;
      }
      return ((((((((((str + "<br /><br />" + "<TABLE BORDER=\"0\" WIDTH=\"100%\" CELLPADDING=\"1\" CELLSPACING=\"0\"><TR><TD bgcolor=\"black\" COLSPAN=\"2\"><FONT face=\"Arial\" color=\"white\"><B> <!--HEADER--></B></FONT></TD></TR></TABLE>".Replace("<!--HEADER-->", "QueryString Collection")) + CollectionToHtmlTable(HttpContext.Current.Request.QueryString)) + "<br /><br />" + "<TABLE BORDER=\"0\" WIDTH=\"100%\" CELLPADDING=\"1\" CELLSPACING=\"0\"><TR><TD bgcolor=\"black\" COLSPAN=\"2\"><FONT face=\"Arial\" color=\"white\"><B> <!--HEADER--></B></FONT></TD></TR></TABLE>".Replace("<!--HEADER-->", "Form Collection")) + CollectionToHtmlTable(HttpContext.Current.Request.Form)) + "<br /><br />" + "<TABLE BORDER=\"0\" WIDTH=\"100%\" CELLPADDING=\"1\" CELLSPACING=\"0\"><TR><TD bgcolor=\"black\" COLSPAN=\"2\"><FONT face=\"Arial\" color=\"white\"><B> <!--HEADER--></B></FONT></TD></TR></TABLE>".Replace("<!--HEADER-->", "Cookies Collection")) + CollectionToHtmlTable(HttpContext.Current.Request.Cookies)) + "<br /><br />" + "<TABLE BORDER=\"0\" WIDTH=\"100%\" CELLPADDING=\"1\" CELLSPACING=\"0\"><TR><TD bgcolor=\"black\" COLSPAN=\"2\"><FONT face=\"Arial\" color=\"white\"><B> <!--HEADER--></B></FONT></TD></TR></TABLE>".Replace("<!--HEADER-->", "Session Variables")) + CollectionToHtmlTable(HttpContext.Current.Session)) + "<br /><br />" + "<TABLE BORDER=\"0\" WIDTH=\"100%\" CELLPADDING=\"1\" CELLSPACING=\"0\"><TR><TD bgcolor=\"black\" COLSPAN=\"2\"><FONT face=\"Arial\" color=\"white\"><B> <!--HEADER--></B></FONT></TD></TR></TABLE>".Replace("<!--HEADER-->", "Server Variables")) + CollectionToHtmlTable(HttpContext.Current.Request.ServerVariables));
    }
  }
}