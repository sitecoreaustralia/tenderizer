﻿#region using

using System;
using System.Linq;
using System.Threading;
using SolrNet;
using SolrNet.Commands.Parameters;
using Tenderize.Framework.Solr;
using Tenderize.Models;


#endregion

namespace Tenderize.Framework.Helpers
{
  public static class IndexUtil
  {
    private static int _progress;
    private static int _total;
    private static readonly object _reindexLock = new object();

    public static int GetProgress()
    {
      return (!Indexing ? 100 : ((_total <= 0) ? 0 : Convert.ToInt32(Math.Ceiling((_progress/((double) _total))*100.0))));
    }

    public static void ReIndex()
    {
      new Thread(RunReindex).Start();
    }

    private static void RunReindex()
    {
      lock (_reindexLock)
      {
        _progress = 0;
        Indexing = true;
        var entities = new TenderizeDbContext();
        _total = entities.Answers.Count() + entities.Questions.Count();

        ReindexAll(entities);

        Indexing = false;
        _progress = 0;
        _total = 0;
      }
    }

    private static bool ReindexAll(TenderizeDbContext entities)
    {
      var instance = SolrUtil.GetSolrInstance<IRelational>();
      if (!ReindexQuestions(entities, instance))
        return false;
      if (!ReindexAnswers(entities, instance))
        return false;
      instance.Commit();
      return true;
    }

    private static bool ReindexAnswers(TenderizeDbContext entities, ISolrOperations<IRelational> instance)
    {
      var errorCount = 0;
      foreach (var answer in entities.Answers)
      {
        if (errorCount > 100)
          return false;
        try
        {
          AddEntityToIndex(instance, answer);
        }
        catch (Exception ex)
        {
          errorCount++;
        }
      }
      instance.Commit();
      return true;
    }

    private static bool ReindexQuestions(TenderizeDbContext entities, ISolrOperations<IRelational> instance)
    {
      var errorCount = 0;
      foreach (var question in entities.Questions)
      {
        if (errorCount > 100)
          return false;
        try
        {
          AddEntityToIndex(instance, question);
        }
        catch (Exception ex)
        {
          errorCount++;
        }
      }
      return true;
    }


    private static void AddEntityToIndex(ISolrOperations<IRelational> instance, IRelational entity)
    {
      if (!IsTextIsIndex(entity.Text))
      {
        if (entity.SolrBoost == null)
          instance.Add(entity);
        else
          instance.AddWithBoost(entity, entity.SolrBoost.Value);
        if (_progress % 100 == 0)
          instance.Commit();
      }
      _progress++;
    }

    private static bool IsTextIsIndex(string text)
    {
      var instance = SolrUtil.GetSolrInstance<SolrResult>();
      var options = new QueryOptions
      {
        Rows = 1
      };
      try
      {
          return instance.Query(new SolrQueryByField("content", text), options).Any();
      } catch (Exception ex){
          return false;
      }
    }

    public static bool Indexing { get; private set; }
  }
}