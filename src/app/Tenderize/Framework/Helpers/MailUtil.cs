﻿using System;
using System.Linq;
using System.Net.Mail;
using Tenderize.Framework.Configuration;

namespace Tenderize.Framework.Helpers
{
    public static class MailUtil
    {
        public static bool SendMail(MailMessage msg)
        {
            var flag = false;
            var client = new SmtpClient();
            //if (client.Host == "localhost")
            //{
            //    client.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            //}
            try
            {
                client.Send(msg);
                flag = true;
            }
            catch
            {
                try
                {
                    client.Send(msg);
                    flag = true;
                }
                catch
                {
                    flag = false;
                }
            }
            return flag;
        }

        /// <summary>
        /// Send email to admin addresses in configuration
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public static bool SendMail(string subject, string body)
        {
            var addresses = Settings.AdminEmailToAddress.Split(new [] {'|'}, StringSplitOptions.RemoveEmptyEntries).Select(a => new MailAddress(a)).ToArray();
            return SendMail(addresses, new MailAddress(Settings.DefaultEmailFromAddress), subject, body);
        }

        public static bool SendMail(string to, string subject, string body)
        {
          return SendMail(to, Settings.DefaultEmailFromAddress, subject, body);
        }

        public static bool SendMail(string to, string from, string subject, string body)
        {
            var toAddress = new MailAddress(to);
            var fromAddress = new MailAddress(from);
            return SendMail(toAddress, fromAddress, subject, body);
        }

        public static bool SendMail(MailAddress to, MailAddress from, string subject, string body)
        {
            return SendMail(new[] {to}, from, subject, body);
        }

        public static bool SendMail(MailAddress[] to, MailAddress from, string subject, string body)
        {
            var msg = new MailMessage
            {
                From = from,
                Body = body,
                Subject = subject,
                IsBodyHtml = true
            };
            foreach (var address in to)
            {
                msg.To.Add(address);
            }
            return SendMail(msg);
        }
    }
}

