﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tenderize.Framework.Configuration
{
    public static class Strings
    {
        public static string ContributorRole = "contributor";
        public static string AdminRole = "admin";
    }
}