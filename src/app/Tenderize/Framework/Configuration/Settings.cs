﻿#region using

using System.Configuration;

#endregion

namespace Tenderize.Framework.Configuration
{
  public static class Settings
  {
    public static string AdminEmailToAddress
    {
      get { return GetSetting("AdminEmailToAddress", "dmg@sitecore.net|the@sitecore.net"); }
    }

    public static string DefaultFromEmail
    {
      get { return GetSetting("DefaultEmailFromAddress", "tenderize@sitecoreaustralia.com.au"); }
    }

    public static string ErrorEmailSubject
    {
      get { return GetSetting("ErrorEmailSubject", "Applicaiton Error"); }
    }

    public static bool ErrorEmailEnabled
    {
      get { return GetSetting("ErrorEmailEnabled", "false") == "true"; }
    }

    public static string DefaultEmailFromAddress
    {
      get { return GetSetting("DefaultEmailFromAddress", "dmg@sitecore.net"); }      
    }

    public static string SolrUrl
    {
      get { return GetSetting("SolrUrl", "http://localhost:8983/solr"); }
    }

    private static string GetSetting(string key, string defaultValue)
    {
      return (ConfigurationManager.AppSettings[key] ?? defaultValue);
    }
  }
}