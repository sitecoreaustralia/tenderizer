Use [TenderizeV3]
INSERT INTO Answers([Text], Collapse, OldId)
SELECT v2.[Text], v2.Collapse, v2.Id
FROM TenderizeV2_Orig.dbo.Answers v2

Go
INSERT INTO Questions ([Text], Collapse, OldID)
SELECT v2.[Text], v2.Collapse, v2.Id
FROM TenderizeV2_Orig.dbo.Questions v2

GO
INSERT INTO Tenders (Name, [Date], OldId)
SELECT v2.Name, v2.[Date], v2.Id
FROM TenderizeV2_Orig.dbo.Tenders v2

Go
INSERT INTO Tags([Text], Popularity, OldId)
SELECT v2.[Text], v2.Popularity, v2.Id
FROM TenderizeV2_Orig.dbo.Tags v2

Go
INSERT INTO AnswerTag(AnswerId, TagId)
SELECT a.Id, t.Id
FROM Answers a
INNER JOIN TenderizeV2_Orig.dbo.AnswerTag at
ON a.OldId = at.Answers_Id
INNER JOIN Tags t
ON t.OldId = at.Tags_Id 

Go
INSERT INTO QuestionTag(QuestionId, TagId)
SELECT q.Id, t.Id
FROM Questions q
INNER JOIN TenderizeV2_Orig.dbo.QuestionTag qt
ON q.OldId = qt.Questions_Id
INNER JOIN Tags t
ON t.OldId = qt.Tags_Id 

Go
INSERT INTO QuestionAnswer(QuestionId, AnswerId)
SELECT q.Id, a.Id
FROM Questions q
INNER JOIN TenderizeV2_Orig.dbo.Relations r
ON q.OldID = r.Questions_Id
INNER JOIN Answers a
ON a.OldId = r.Answers_Id

Go
INSERT INTO TenderQuestions(QuestionAnswerId, TenderId, TenderSequence, ExcelQuestionLocation, ExcelAnswerLocation)
SELECT qa.Id, t.Id, oldQ.TenderSequence, oldQ.ExcelLocation, oldA.ExcelLocation
FROM TenderizeV2_Orig.dbo.Questions oldQ
INNER JOIN Tenders t ON 
t.OldId = oldQ.TenderId
INNER JOIN Questions q ON
q.OldID = oldQ.Id
INNER JOIN QuestionAnswer qa
ON q.Id = qa.QuestionId
INNER JOIN Answers a
ON qa.AnswerId = a.Id
INNER JOIN TenderizeV2_Orig.dbo.Answers oldA
ON a.OldId = oldA.Id

Go
INSERT INTO Votes(Score, AnswerId, Username)
SELECT v2.Score, a.Id, v2.Username
FROM TenderizeV2_Orig.dbo.Votes v2
INNER JOIN Answers a
on a.OldId = v2.AnswerId